﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace VintraTest.Types
{
    class HTTPValidationError
    {
        [JsonProperty("detail")]
        public string Detail { get; set; }
    }
}
