﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace VintraTest.Types
{
    public partial class User
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("full_name")]
        public string FullName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("disabled")]
        public bool Disabled { get; set; }
    }
}
