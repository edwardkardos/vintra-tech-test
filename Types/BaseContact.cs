﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace VintraTest.Types
{
    public class BaseContact
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("mobile")]
        public string Mobile { get; set; }
    }
}
