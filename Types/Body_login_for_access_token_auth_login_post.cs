﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace VintraTest.Types
{
    public class Body_login_for_access_token_auth_login_post
    {
        public Body_login_for_access_token_auth_login_post(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public string ToURLEncoded() 
        {
            return $"username={Username}&password={Password}&scope={Scope}&client_id={Client_ID}&client_secret={Client_Secret}";
        }

        [JsonProperty("grant_type", NullValueHandling = NullValueHandling.Ignore)]
        public string Grant_Type { get; set; }

        [JsonProperty("username", NullValueHandling = NullValueHandling.Ignore)]
        public string Username { get; set; }

        [JsonProperty("password", NullValueHandling = NullValueHandling.Ignore)]
        public string Password { get; set; }

        [JsonProperty("scope", NullValueHandling = NullValueHandling.Ignore)]
        public string Scope { get; set; }

        [JsonProperty("client_id", NullValueHandling = NullValueHandling.Ignore)]
        public string Client_ID { get; set; }

        [JsonProperty("client_secret", NullValueHandling = NullValueHandling.Ignore)]
        public string Client_Secret { get; set; }
    }
}
