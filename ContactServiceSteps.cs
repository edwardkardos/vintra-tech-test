﻿using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using VintraTest.Types;

namespace VintraTest
{
    public partial class ContactServiceTests
    {
        async Task<BaseContact> When_I_Search_Contacts_By_Name(BaseContact search)
        {
            // WHEN I GET from /api/v1/contacts/search with a FirstName or LastName
            TestContext.WriteLine("Searching contacts by name...");

            string result = await When_I_GET_From_endpoint("/api/v1/contacts/search", JsonConvert.SerializeObject(search));

            TestContext.WriteLine($"Deserializing user response: {result}");

            BaseContact contact = JsonConvert.DeserializeObject<BaseContact>(result);
            return contact;
        }

        async Task<BaseContact> When_I_Update_A_Contact_By_ID(int contactID, BaseContact fields = null)
        {
            // WHEN I PATCH to /api/v1/contacts/{contactID}
            TestContext.WriteLine("Updating contact by ID...");

            if (fields == null)
            {
                fields = new BaseContact() { FirstName = "Different" };
            }
            string payload = JsonConvert.SerializeObject(fields);
            string result = await When_I_PATCH_To_endpoint(payload, $"/api/v1/contacts/{contactID}")
                .ConfigureAwait(false);

            TestContext.WriteLine($"Deserializing user response: {result}");

            BaseContact contact = JsonConvert.DeserializeObject<BaseContact>(result);
            return contact;
        }
        async Task<BaseContact> When_I_Delete_A_Contact_By_ID(int contactID)
        {
            // WHEN I DELETE from /api/v1/contacts/{contactID}
            TestContext.WriteLine("Deleting...");
            string result = await When_I_DELETE_From_endpoint($"/api/v1/contacts/{contactID}")
                .ConfigureAwait(false);

            TestContext.WriteLine($"Deserializing user response: {result}");

            BaseContact contact = JsonConvert.DeserializeObject<BaseContact>(result);
            return contact;

        }
        async Task<BaseContact> When_Get_A_Contact_By_ID(int contactID)
        {
            // WHEN I GET from /api/v1/contacts/{contactID}
            TestContext.WriteLine("Getting contact by ID...");
            string result = await When_I_GET_From_endpoint($"/api/v1/contacts{contactID}")
                .ConfigureAwait(false);

            TestContext.WriteLine($"Deserializing user response: {result}");

            BaseContact contact = JsonConvert.DeserializeObject<BaseContact>(result);
            return contact;
        }

        async Task<List<BaseContact>> When_I_Get_All_Contacts()
        {
            // WHEN I GET from /api/v1/contacts
            TestContext.WriteLine("Getting authenticated user...");
            string result = await When_I_GET_From_endpoint("/api/v1/contacts")
                .ConfigureAwait(false);

            TestContext.WriteLine($"Deserializing user response: {result}");

            List<BaseContact> contacts = JsonConvert.DeserializeObject<List<BaseContact>>(result);
            return contacts;
        }

        async Task<int> When_I_Create_A_New_Contact(BaseContact newContact)
        {
            // WHEN I POST BaseContact details to /api/v1/contacts
            TestContext.WriteLine("Adding new contact...");
            string result = await When_I_POST_payload_To_endpoint(JsonConvert.SerializeObject(newContact), "/api/v1/contacts")
                .ConfigureAwait(false);

            TestContext.WriteLine($"Deserializing response: {result}");

            return int.Parse(result);
        }

        async Task<Token> When_I_Login_For_The_Access_Token()
        {
            // WHEN I POST credentials to /auth/login
            TestContext.WriteLine("Authenticating with endpoint...");
            var loginBody = new Body_login_for_access_token_auth_login_post(TEST_USER, TEST_PASS).ToURLEncoded();
            string result = await When_I_POST_payload_To_endpoint(loginBody, "/auth/login")
                .ConfigureAwait(false);

            TestContext.WriteLine($"Deserializing auth response: {result}");
            token = JsonConvert.DeserializeObject<Token>(result);
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.AccessToken);
            return token;
        }

        async Task<User> When_I_Get_The_Authenticated_User()
        {
            // WHEN I GET from /auth/me
            TestContext.WriteLine("Getting authenticated user...");
            string result = await When_I_GET_From_endpoint("/auth/me")
                .ConfigureAwait(false);
            TestContext.WriteLine(result);

            TestContext.WriteLine($"Deserializing user response: {result}");
            User user = JsonConvert.DeserializeObject<User>(result);
            return user;
        }

        async Task<bool> When_I_Logout()
        {
            // WHEN I GET from /auth/logout
            TestContext.WriteLine("Logging out authenticated user...");
            string result = await When_I_GET_From_endpoint("/auth/logout")
                .ConfigureAwait(false);
            TestContext.WriteLine(result);

            if (result.Contains("Incorrect username or conflicted credentials"))
            {
                throw new HttpRequestException($"/auth/logout: {result}");
            }

            TestContext.WriteLine($"Deserializing user response: {result}");
            return true;
        }

        async Task<string> When_I_Send_A_HTTP_Request(HttpRequestMessage request)
        {
            // Attach JWT if authenticated
            if (token != null)
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.AccessToken);
            }

            // Send request
            TestContext.WriteLine($"Getting payload from {request.RequestUri}");
            string result = await (await client.SendAsync(request).ConfigureAwait(false))
                .Content.ReadAsStringAsync().ConfigureAwait(false);
            CheckForHTTPErrors(result);

            return result;
        }

        async Task<string> When_I_POST_payload_To_endpoint(string payload, string endpoint)
        {
            // Form auth request
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, endpoint)
            {
                Content = new StringContent(
                        payload,
                        Encoding.UTF8,
                        "application/x-www-form-urlencoded")
            };

            return await When_I_Send_A_HTTP_Request(request);
        }

        async Task<string> When_I_GET_From_endpoint(string endpoint)
        {
            // Form logon request
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, endpoint);

            return await When_I_Send_A_HTTP_Request(request);
        }

        async Task<string> When_I_DELETE_From_endpoint(string endpoint)
        {
            // Form logon request
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, endpoint);

            return await When_I_Send_A_HTTP_Request(request);
        }

        async Task<string> When_I_GET_From_endpoint(string endpoint, string payload)
        {
            // Form logon request
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, endpoint)
            {
                Content = new StringContent(
                        payload,
                        Encoding.UTF8,
                        "application/x-www-form-urlencoded")
            };

            return await When_I_Send_A_HTTP_Request(request);
        }

        async Task<string> When_I_PATCH_To_endpoint(string payload, string endpoint)
        {
            // Form auth request
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Patch, endpoint)
            {
                Content = new StringContent(
                        payload,
                        Encoding.UTF8,
                        "application/x-www-form-urlencoded")
            };

            return await When_I_Send_A_HTTP_Request(request);
        }

        void CheckForHTTPErrors(string response)
        {
            HTTPValidationError error = JsonConvert.DeserializeObject<HTTPValidationError>(response);
            if (error.Detail != null)
            {
                throw new HttpRequestException(error.Detail);
            }
        }
    }
}
