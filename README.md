## Vintra Technical Assignment - ##
**Senior QA (Edward Kardos)**

Thank you for taking the time to review the technical assignment for the Senior QA role.

I have interptetted the provided Project Requirement/User Stories into Behavior-Driven Development (BDD) style scenarios. This allows a user-focused, readable, re-usable testing. This is implemented in a NUnit based test framework in NET Core 3.1 (C#), making it entirely portable. You will need NET Core to build and run the binaries. If importing the solution or project into another IDE, tests should be discovered by any test runner, otherwise could be run directly from the produced DLL (there is a Windows x64 Binary included):

**NOTE:** Before building or running the tests, the URL variable for the Dockerized service host, lives at the top of the *ContactServiceTests.cs* file.

**To build:** *dotnet build VintraTest.csproj*

**To run tests:** *dotnet vstest VintraTest.dll*

These tests are implemented from scratch, heavily utilizing system HttpClient libraries and a Newtonsoft serialisation library. Project structure is simple:

- *ContactServiceTests.cs* - Test definitions and data

- *ContactServiceSteps.cs* - Automation and helper logic in BDD style

- *Types/..* - Any schemas/types for quick serialisation to/from JSON

The test suite provided is complete based on the requirements, but could be further parameterized to test edgier cases. I have embellished these test cases in a couple of instances in the framework provided.

## Issues Encountered ##
Unfortunately I did encounter some errors authenticating with the Contacts API itself. As the tests attached will show, authentication processes appear to be functioning, however the framework was unable to authenticate with the Contacts API using the provided JWT, reporting the undocumented error "Not authenticated". **After confirming using Fiddler/Charlesproxy that the JWT is indeed being passed appropriately, I have deduced there might be an issue with the provided testing environment. For this reason, while I have completed the test suite, I have not been able to validate results for those APIs, with those test failing (appropriately).**

Other issues observed worth noting (which were unblocked to test):

- Requirement AUTH-03, implemented in test AUTH_03_Can_Logout, is failing throwing: *Incorrect username or conflicted credentials*




**I am looking forward to Vintra's feedback. Thanks again for your time.**
