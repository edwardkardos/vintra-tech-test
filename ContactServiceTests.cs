using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using VintraTest.Types;

namespace VintraTest
{
    public partial class ContactServiceTests
    {
        const string TARGET_HOST_URL = "http://192.168.0.19:8000/";
        const string TEST_USER = "test";
        const string TEST_PASS = "1234";

        HttpClient client;
        Token token;

        [SetUp]
        public async Task Setup()
        {
            TestContext.WriteLine("Setting up client for test...");
            client = new HttpClient();
            client.BaseAddress = new Uri(TARGET_HOST_URL);
            await When_I_Login_For_The_Access_Token();
        }

        [TearDown]
        public void Teardown()
        {
            client.Dispose();
        }

        /// <summary>
        /// AUTH_01 As a user, I want to be able to authenticate myself against the API
        /// (JWT) using /auth/login endpoint, so that I can use the contacts API.
        /// 
        /// Any user starting with �test�
        /// and password �1234� is
        /// automatically authenticated.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task AUTH_01_Can_Authenticate_With_Contacts_API()
        {
            // When I authenticate with the contacts API
            // Then I should receive a token (JWT)
            Assert.IsNotNull(token);
            TestContext.WriteLine($"Successfully received {token.TokenType} JWT {token.AccessToken}");
        }

        /// <summary>
        /// AUTH_02 As a previously logged-in user, I want to be able to check my
        /// authentication credentials against /auth/me endpoint, so that I can
        /// check if I am properly logged in the API.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task AUTH_02_Can_Validate_Authentication()
        {
            // When I authenticate with the contacts API
            // AND When I get the authenticated user
            // Then the authenticated user should match my credentials
            Assert.IsTrue(When_I_Get_The_Authenticated_User().Result.Username == TEST_USER);
            TestContext.WriteLine($"Successfully received {token.TokenType} JWT {token.AccessToken}");
        }

        /// <summary>
        /// AUTH_03 As a previously logged-in user, I want to be able to logout from the
        /// API using /auth/logout endpoint, so that I can no longer use the API.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task AUTH_03_Can_Logout()
        {
            // WHEN I authenticate with the contacts API
            // AND WHEN I logout
            await When_I_Logout();

            // THEN I should no longer be authenticated
            var exception = Assert.Catch(async () =>
            {
                await When_I_Get_The_Authenticated_User();
            },
            "Not implemented");

            TestContext.WriteLine("Successfully caught error: " + exception.ToString());
        }

        /// <summary>
        /// CON_01 As a previously logged-in user, I want to be allowed to store new
        /// contact data, so that I can create new contacts in the Contacts API
        /// and get an unique id.
        /// 
        /// string firstName (mandatory)
        /// string lastName (mandatory)
        /// string email (optional)
        /// string phone (optional)
        /// string mobile (optional)
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task CON_01_01_Can_Store_New_Contact_And_Get_ID_With_Mandatory_Fields()
        {
            // WHEN I authenticate with the contacts API

            // AND WHEN I create a new contact with only mandatory fields
            BaseContact newContact = new BaseContact()
            {
                FirstName = "John",
                LastName = "Doe"
            };
            object result = await When_I_Create_A_New_Contact(newContact);

            // THEN I should recieve a unique ID for the user
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// CON_01 As a previously logged-in user, I want to be allowed to store new
        /// contact data, so that I can create new contacts in the Contacts API
        /// and get an unique id.
        /// 
        /// string firstName (mandatory)
        /// string lastName (mandatory)
        /// string email (optional)
        /// string phone (optional)
        /// string mobile (optional)
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task CON_01_02_Can_Store_New_Contact_And_Get_ID_With_All_Fields()
        {
            // WHEN I authenticate with the contacts API

            // AND WHEN I create a new contact with all fields
            BaseContact newContact = new BaseContact()
            {
                FirstName = "John",
                LastName = "Doe",
                Email = "test@test.com",
                Mobile = "00000000000",
                Phone = "00000000000"
            };
            object result = await When_I_Create_A_New_Contact(newContact);

            // THEN I should recieve a unique ID for the user
            Assert.IsNotNull(result);
        }

        [Test]
        public async Task CON_01_03_Cannot_Store_New_Contact_And_Get_ID_With_No_Mandatory_Fields()
        {
            // WHEN I authenticate with the contacts API

            // AND WHEN I create a new contact without the mandatory fields
            BaseContact newContact = new BaseContact()
            {
                Email = "test@test.com",
                Mobile = "00000000000",
                Phone = "00000000000"
            };
            object result = await When_I_Create_A_New_Contact(newContact);

            // THEN I should recieve a unique ID for the user
            int id;
            Assert.True(int.TryParse((string)result, out id));
        }

        /// <summary>
        /// CON_02 As a user, I want to not allow two entries with the same firstName
        /// and lastName, so that I cannot have duplicates in my agenda.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task CON_02_Cannot_Add_Duplicate_First_And_Last_Names()
        {
            // WHEN I authenticate with the contacts API
            // AND WHEN I create a contact
            BaseContact newContact = new BaseContact()
            {
                FirstName = "Duplicate",
                LastName = "Names"
            };
            object result = await When_I_Create_A_New_Contact(newContact);

            // AND WHEN I create a contact with the same first and last name
            // THEN I should receive an error
            var exception = Assert.Catch( async () => 
            {
                await When_I_Create_A_New_Contact(newContact);
            },
                "Not implemented");

            TestContext.WriteLine("Successfully caught error: " + exception.ToString());
        }

        /// <summary>
        /// CON_03 As a previously logged-in user, I want to retrieve all previously
        /// created contacts, so I can retrieve my full agenda.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task CON_03_Can_Retrieve_All_Contacts()
        {
            // WHEN I authenticate with the contacts API

            // AND WHEN I get all contacts
            List<BaseContact> results = await When_I_Get_All_Contacts();

            // THEN I should recieve a list of contacts previously added
            Assert.IsNotNull(results);
        }

        /// <summary>
        /// CON_04 As a previously logged-in user, I want to retrieve previously created
        /// contact by unique id, so I can retrieve a specific agenda entry.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task CON_04_Can_Retrieve_Previous_Contact_By_ID()
        {
            // WHEN I authenticate with the contacts API

            // AND WHEN I add a contact
            BaseContact newContact = new BaseContact()
            {
                FirstName = "CanUpdateFieldOfExistingContact",
                LastName = "Test"
            };
            int contactID = await When_I_Create_A_New_Contact(newContact);

            // AND WHEN I get a contact by ID
            BaseContact contact = await When_Get_A_Contact_By_ID(contactID);
            
            // THEN I should get the contact information for the contact matching the ID
            Assert.IsNotNull(contact);
        }

        /// <summary>
        /// CON_05 As a previously logged-in user, I want to update any field of a
        /// previously created contact by unique id, so I can keep my agenda
        /// up to date.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task CON_05_Can_Update_Field_Of_Existing_Contact()
        {
            // WHEN I authenticate with the contacts API

            // AND WHEN I add a contact
            BaseContact newContact = new BaseContact()
            {
                FirstName = "CanUpdateFieldOfExistingContact",
                LastName = "Test"
            };
            int contactID = await When_I_Create_A_New_Contact(newContact);

            // AND WHEN I update a contact by ID
            BaseContact fields = new BaseContact() { FirstName = "CanUpdateFieldOfExistingContactDifferent" };
            await When_I_Update_A_Contact_By_ID(contactID, fields);

            // AND WHEN I get the updated version of the contact
            BaseContact updatedContact = await When_Get_A_Contact_By_ID(contactID);

            // THEN it should match the updated name
            Assert.Equals(fields.FirstName, updatedContact.FirstName);
        }

        /// <summary>
        /// CON_06 As a previously logged-in user, I want to search any contacts by
        /// firstName and/or lastName, so that I can quickly find any contact by
        /// name in the agenda.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task CON_06_01_Can_Search_By_First_Name()
        {
            // WHEN I authenticate with the contacts API

            // AND WHEN I add a contact
            BaseContact newContact = new BaseContact()
            {
                FirstName = "SearchByFirstName",
                LastName = "Test"
            };
            await When_I_Create_A_New_Contact(newContact);

            // AND WHEN I search by the contact's first name
            BaseContact contact = await When_I_Search_Contacts_By_Name(new BaseContact() { FirstName = newContact.FirstName });

            // THEN if its unique, I should receive the data for that contact
            Assert.NotNull(contact);
        }

        /// <summary>
        /// CON_06 As a previously logged-in user, I want to search any contacts by
        /// firstName and/or lastName, so that I can quickly find any contact by
        /// name in the agenda.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task CON_06_02_Can_Search_By_Last_Name()
        {
            // WHEN I authenticate with the contacts API

            // AND WHEN I add a contact
            BaseContact newContact = new BaseContact()
            {
                FirstName = "Test",
                LastName = "SearchByLastName"
            };
            await When_I_Create_A_New_Contact(newContact);

            // AND WHEN I search by the contact's last name
            BaseContact contact = await When_I_Search_Contacts_By_Name(new BaseContact() { LastName = newContact.LastName });

            // THEN if its unique, I should receive the data for that contact
            Assert.NotNull(contact);
        }

        /// <summary>
        /// CON_07 As a previously logged-in user, I want to delete any contact by
        /// unique id, so that I can delete unused contacts and keep my agenda
        /// up to date.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task CON_07_Can_Delete_Contact_By_ID()
        {
            // WHEN I authenticate with the contacts API

            // AND WHEN I add a contact
            BaseContact newContact = new BaseContact()
            {
                FirstName = "Test",
                LastName = "SearchByLastName"
            };
            int contactID = await When_I_Create_A_New_Contact(newContact);

            // AND WHEN I delete a contact by ID
            await When_I_Delete_A_Contact_By_ID(contactID);

            // AND WHEN I get the contact by ID
            // THEN the contact should no longer exist
            BaseContact deletedContact;
            var exception = Assert.Catch(async () =>
            {
                deletedContact = await When_Get_A_Contact_By_ID(contactID);
            },
                "Not implemented");

            TestContext.WriteLine("Successfully caught error: " + exception.ToString());
        }

        /// <summary>
        /// SEC_01 As a logged-out user, I want to be rejected at any contacts
        /// management endpoint, so that I can be sure my agenda is secure.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task SEC_01_Cannot_Reach_Contact_Endpoints_While_Logged_Out()
        {
            // WHEN I authenticate with the contacts API

            // AND WHEN I log out
            await When_I_Logout();

            // AND WHEN I try to use the contacts API
            // THEN I should get an error stating I am not authenticated
            var exception = Assert.Catch(async () =>
            {
                await When_I_Get_All_Contacts();
            },
                "Not implemented");

            TestContext.WriteLine("Successfully caught error: " + exception.ToString());
        }
    }
}
